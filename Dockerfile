FROM docker.io/azul/zulu-openjdk-alpine:17.0.2

RUN apk update && apk del wireshark && \
	rm -rf /var/cache/apk/* && \
	rm -rf /tmp/* && \
	adduser -h /home/java -s /sbin/nologin -u 10001 -D java


USER java
